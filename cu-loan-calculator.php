<?php
/**
 * @package CuLoanCalculator
 */
/*
Plugin Name: CU Loan Calculator
Plugin URI: https://bitbucket.org/passionforcreative/loan-calculator-plugin/src/master/
Description: Bespoke Loan Calculator made by Passion for Creative.
Version: 1.0.0
Author: Darragh Power
Author URI: http://www.passionforcreative.com/
License: GPLv2 or later
Text Domain: cu-loan-calculator
*/

add_action( 'admin_init', 'child_plugin_has_parent_plugin' );
function child_plugin_has_parent_plugin() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
        add_action( 'admin_notices', 'child_plugin_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function child_plugin_notice(){
    ?><div class="error"><p>CU Loan Calulator requires <a href="https://www.advancedcustomfields.com/pro/#pricing-table" target="_blank">Advanced Custom Fields PRO</a> installed and activated</p></div><?php
}

//Enqueue
function cu_loan_calculator_scripts() {
   wp_register_style ( 'style', plugins_url ( 'css/style.css', __FILE__ ) );
   wp_register_style ( 'jquery-ui-css', plugins_url ( 'css/jquery-ui.min.css', __FILE__ ) );
   wp_register_style ( 'pretty-dropdowns-css', plugins_url ( 'css/prettydropdowns.css', __FILE__ ) );
   wp_register_style ( 'bootstrap-min-css', plugins_url ( 'css/bootstrap.min.css', __FILE__ ) );
   wp_register_script ( 'pretty-dropdowns-js', plugins_url ( 'js/jquery.prettydropdowns.js', __FILE__ ) , array('jquery'), '', true );
   wp_register_script ( 'main', plugins_url ( 'js/main.js', __FILE__ ) , array('jquery'), '', true  );


   wp_enqueue_style('style');
   wp_enqueue_style('jquery-ui-css');
   wp_enqueue_style('pretty-dropdowns-css');
   wp_enqueue_style('bootstrap-min-css');
   wp_enqueue_script('pretty-dropdowns-js');
   wp_enqueue_script('main');
}

add_action( 'wp_enqueue_scripts', 'cu_loan_calculator_scripts' );

// Register Loan Calculator in Dashboard
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page( array(

	'page_title' 	=> 'Loan Calculator',
	'menu_title' 	=> 'Loan Calculator',
	'menu_slug' 	=> 'loan-calculator',
	'icon_url' => 'dashicons-plus-alt',
	'position' => 9

	) );
	
}

// Calculator Fields
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5b3376b1001c5',
	'title' => 'Loan Calculator',
	'fields' => array(
		array(
			'key' => 'field_5b3376ba1543a',
			'label' => 'Loan Calculator',
			'name' => 'loan_calculator',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5b3377031543b',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Add Loan Type',
			'sub_fields' => array(
				array(
					'key' => 'field_5b3377031543b',
					'label' => 'Loan Type',
					'name' => 'loan_type',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33770a1543c',
					'label' => 'Rate',
					'name' => 'rate',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33771f1543d',
					'label' => 'APR',
					'name' => 'apr',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33775d1543e',
					'label' => 'Loan Amount (min)',
					'name' => 'loan_amount_min',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b3377671543f',
					'label' => 'Loan Amount (mid)',
					'name' => 'loan_amount_mid',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33777115440',
					'label' => 'Loan Amount (max)',
					'name' => 'loan_amount_max',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33777615441',
					'label' => 'Loan Term (min)',
					'name' => 'loan_term_min',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33778215442',
					'label' => 'Loan Term (mid)',
					'name' => 'loan_term_mid',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5b33779315443',
					'label' => 'Loan Term (max)',
					'name' => 'loan_term_max',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'loan-calculator',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


function calculator(){

   include_once(dirname(__FILE__) . '/includes/calculator.php'); 	

}

add_shortcode( 'calculator', 'calculator' );