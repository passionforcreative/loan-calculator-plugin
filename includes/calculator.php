<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<!-- Loan Calculator -->
<div id="cu-loan-calculator">
   <?php
      if( have_rows('loan_calculator' , 'option') ):
         // loop through the rows of data
      
         echo '<fieldset>';
         echo '<select name="loan-menu" id="loan-menu" class="ui-menu">';
      
         $i = 1; 
          while ( have_rows('loan_calculator' , 'option') ) : the_row();
      
          //variables
       $loan_type = get_sub_field('loan_type'); 
       $rate = get_sub_field('rate'); ?>
   <option value="<?php echo $i; ?>"><?php echo $loan_type; ?> - <?php echo $rate; ?>%</option>
   <?php $i++; 
      endwhile;
      echo '</select>';
      echo '</fieldset>';
      endif;
      ?>
   <?php
      if( have_rows('loan_calculator' , 'option') ):
       
       $i = 1; 
       while ( have_rows('loan_calculator' , 'option') ) : the_row();
       $loan_type = get_sub_field('loan_type');
       $rate = get_sub_field('rate');
       $apr = get_sub_field('apr');
       $la_min = get_sub_field('loan_amount_min');
       $la_mid = get_sub_field('loan_amount_mid');
       $la_max = get_sub_field('loan_amount_max');
       $lt_min = get_sub_field('loan_term_min');
       $lt_mid = get_sub_field('loan_term_mid');
       $lt_max = get_sub_field('loan_term_max');
      ?>
   <div class="<?php if ($i != 1) { echo 'hidden'; } ?> loan-select-<?php echo $i; ?>">
      <div class="row">
         <div class="col-lg-5">
            <div class="form-group">
               <p>Loan Amount <b class="slider-label">€<span class="loan-amount-label-<?php echo $i; ?>"></span></b></p>
               <div id="loan-amount-slider-<?php echo $i; ?>"></div>
               <div class="mobile-controls">
                  <div id="minus-amount-<?php echo $i; ?>" class="minus">-</div>
                  <div id="plus-amount-<?php echo $i; ?>" class="plus">+</div>
               </div>
               <input type="text" id="loan-amount-<?php echo $i; ?>" value="<?php echo $la_mid; ?>" disabled/>
            </div>
            <div class="form-group">
               <p>Loan Term <b class="slider-label"><span class="loan-term-label-<?php echo $i; ?>"></span> month(s)</b></p>
               <div id="loan-term-slider-<?php echo $i; ?>"></div>
               <div class="mobile-controls">
                  <div id="minus-term-<?php echo $i; ?>" class="minus">-</div>
                  <div id="plus-term-<?php echo $i; ?>" class="plus">+</div>
               </div>
               <input type="text" id="loan-term-<?php echo $i; ?>" value="<?php echo $lt_mid; ?>" disabled/>
            </div>
         </div>
         <div class="col-lg-6 offset-lg-1">
            <input id="monthly-repayments-<?php echo $i; ?>" type="text" disabled/>
            <input id="total-repayments-<?php echo $i; ?>" type="text" disabled/>
            <div class="row">
               <div class="col-6">
                  <p>Monthly Repayments <br><b>€<span class="monthly-repayments-label-<?php echo $i; ?>"></span></b></p>
                  <p>Loan Term <br><b><span class="loan-term-label-<?php echo $i; ?>"></span> month(s)</b></p>
               </div>
               <div class="col-6">
                  <p>Total Repayments <br><b>€<span class="total-repayments-label-<?php echo $i; ?>"></span></b></p>
                  <p>Interest <br><b>€<span class="total-interest-label-<?php echo $i; ?>"></span></b></p>
               </div>
            </div>
         </div>
      </div>
      <!-- End Row -->
      <div id="representitive-example">
         <h4>Representitive Example</h4>
         <p>
            €<span class="loan-amount-label-<?php echo $i; ?>"></span> over <span class="loan-term-label-<?php echo $i; ?>"></span> month(s) at a variable rate of <span class="rate-label-<?php echo $i; ?>"></span>% (<span class="apr-label-<?php echo $i; ?>"></span>%) equates to monthly repayments of €<span class="monthly-repayments-label-<?php echo $i; ?>"></span>
            and a total repayable of €<span class="total-repayments-label-<?php echo $i; ?>"></span>
         </p>
      </div>
   </div>
   <script type="text/javascript">
      jQuery(function($) {
          //Function to calculate loan
          var loanRate = <?php echo $rate; ?> ;
          var apr = <?php echo $apr ?> ;
          var laMin = <?php echo $la_min ?> ;
          var laMid = <?php echo $la_mid ?> ;
          var laMax = <?php echo $la_max ?> ;
          var ltMin = <?php echo $lt_min ?> ;
          var ltMid = <?php echo $lt_mid ?> ;
          var ltMax = <?php echo $lt_max ?> ;

          $(document).ready(function() {
              function calculate() {
                rate = loanRate;
                interest = rate / 1200;
                IR = interest + 1;
                payments = $("#loan-term-<?php echo $i; ?>").val();
                IR = Math.pow(IR, payments);
                principal = $("#loan-amount-<?php echo $i; ?>").val();
                fig1 = principal * IR;
                fig2 = IR - 1;
                fig3 = fig2 / interest;
                monthly = fig1 / fig3;
                monthlyRound = monthly.toFixed(2);
                totalPayments = monthly * payments;
                totalPaymentsRound = totalPayments.toFixed(2);
                totalInterest = totalPaymentsRound - principal;
                totalInterestRound = totalInterest.toFixed(2);
      
                $("#monthly-repayments-<?php echo $i; ?>").val(monthlyRound);
                $("#total-repayments-<?php echo $i; ?>").val(totalPaymentsRound);
      
                //Adding commas to figures
                $.fn.digits = function(){ 
                    return this.each(function(){ 
                        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
                    })
                }
      
                //Labels
                $(".loan-amount-label-<?php echo $i; ?>").text(principal).digits();
                $(".rate-label-<?php echo $i; ?>").text(rate);
                $(".loan-term-label-<?php echo $i; ?>").text(payments);
                $(".apr-label-<?php echo $i; ?>").text(apr);
                $(".monthly-repayments-label-<?php echo $i; ?>").text(monthlyRound).digits();
                $(".total-repayments-label-<?php echo $i; ?>").text(totalPaymentsRound).digits();
                $(".total-interest-label-<?php echo $i; ?>").text(totalInterestRound).digits();
      
      
                //Representitive Labels
              
              }
      
              //Loan Amount Slider
              $("#loan-amount-slider-<?php echo $i; ?>").slider({
                  range: "max",
                  max: laMax,
                  min: laMin,
                  value: laMid,
                  step: 1000,
                  slide: function(event, ui) {
      
                      $("#loan-amount-<?php echo $i; ?>").val(ui.value);
                      calculate();
                  }
              });

              $("#plus-amount-<?php echo $i; ?>").click(function () {
                var value = $("#loan-amount-slider-<?php echo $i; ?>").slider("value");
                var step = $("#loan-amount-slider-<?php echo $i; ?>").slider("option", "step");

                $("#loan-amount-slider-<?php echo $i; ?>").slider("value", value + step);
                $("#loan-amount-<?php echo $i; ?>").val(value + step);

                calculate();

              });

              $("#minus-amount-<?php echo $i; ?>").click(function () {
                var value = $("#loan-amount-slider-<?php echo $i; ?>").slider("value")
                var step = $("#loan-amount-slider-<?php echo $i; ?>").slider("option", "step");

                $("#loan-amount-slider-<?php echo $i; ?>").slider("value", value - step);
                $("#loan-amount-<?php echo $i; ?>").val(value - step);
                calculate();
             
              });


              //Loan Term Slider
              $("#loan-term-slider-<?php echo $i; ?>").slider({
                  range: "max",
                  max: ltMax,
                  min: ltMin,
                  value: ltMid,
                  slide: function(event, ui) {
                      $("#loan-term-<?php echo $i; ?>").val(ui.value);
                      calculate();
                  }
              });

              $("#plus-term-<?php echo $i; ?>").click(function () {
                var value = $("#loan-term-slider-<?php echo $i; ?>").slider("value");
                var step = $("#loan-term-slider-<?php echo $i; ?>").slider("option", "step");

                $("#loan-term-slider-<?php echo $i; ?>").slider("value", value + step);
                $("#loan-term-<?php echo $i; ?>").val(value + step);
                calculate();

              });

              $("#minus-term-<?php echo $i; ?>").click(function () {
                var value = $("#loan-term-slider-<?php echo $i; ?>").slider("value")
                var step = $("#loan-term-slider-<?php echo $i; ?>").slider("option", "step");

                $("#loan-term-slider-<?php echo $i; ?>").slider("value", value - step);
                $("#loan-term-<?php echo $i; ?>").val(value - step);
                calculate();
             
              });
      
              $("#loan-term-<?php echo $i; ?>").val($("#loan-term-slider-<?php echo $i; ?>").slider("value"));
              calculate();
          });
      });
   </script>
   <?php $i++; 
      endwhile;
      endif;
      ?>
</div>
<!-- End Loan Calculator -->
