jQuery(function($) {
    //Loan Calculator Dropdown Menu
    $("#loan-menu").change(function() {
        var value = $("#loan-menu option:selected").val();
        var theDiv = $(".loan-select-" + value);

        theDiv.slideDown().removeClass("hidden");
        theDiv.siblings('[class*=loan-select-]').slideUp(function() {
            $(this).addClass("hidden");
        });
    });

    $(document).ready(function() {
        $('select').prettyDropdown();
    });
});